from functools import reduce

def big_dumb_hashmap_as_a_service(list_of_numbers, num_to_find):
    """
    Run throuhgh the big_dumb_hashmap to see if we can find two numbers that sum to the number
    :param num_to_find:
    :return:
    """

    hashmap = {}
    answer = []

    for number in list_of_numbers:
        hashmap[number] = num_to_find - number

    for key in hashmap.keys():

        try:
            answer = [hashmap[key], hashmap[hashmap[key]]]
            # print(answer)
            break
        except KeyError:
            pass

    return hashmap, answer


def run():
    with open("input.txt", 'r') as f:
        lines = f.readlines()
        lines_as_ints = []
        for line in lines:
            lines_as_ints.append(int(line))

        hashmap, _ = big_dumb_hashmap_as_a_service(lines_as_ints, 2020)

        for key in hashmap.keys():
            new_key = 2020 - key
            _, answer = big_dumb_hashmap_as_a_service(lines_as_ints, new_key)

            if answer:
                print(f'answer list: {key}, {answer[0]} {answer[1]}')
                answer.append(key)
                print(reduce(lambda x, y: x*y, answer))
                break


if __name__ == "__main__":
    run()