big_dumb_hashmap = {}

with open("input.txt", 'r') as f:
    lines = f.readlines()

    for line in lines:
        line_int = int(line)

        big_dumb_hashmap[line_int] = 2020 - line_int

    for key in big_dumb_hashmap.keys():

        try:
            answer = big_dumb_hashmap[key] * big_dumb_hashmap[big_dumb_hashmap[key]]
            print(f'The combo is: {big_dumb_hashmap[key]}, {big_dumb_hashmap[big_dumb_hashmap[key]]}')
            print(f'The answer is {answer}')
            break
        except KeyError:
            pass
